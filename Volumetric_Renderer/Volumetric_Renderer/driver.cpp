/**********************************************
* Author: Samuel Kenney 3/3/2017
*
* Grove City College: Introduction to Graphics
*
* Dr. Boatright
*
* Implements driver for testing Volumetric Rendering class
*
**********************************************/

#include "EasyBMP.h"
#include "VoxelBuffer.h"
#include "ray_generator.h"
#include "ray_marching.h"
#include "volumetric_renderer.h"

ray_marching* test1;
ray_marching* test2;
ray_marching* test3;

int main (int argc, char **argv){
	 
	std::cout << "***** Testing Colored Sphere *****" << std::endl;
	//text file to read from
	test1 = ray_marching::factory("coloredSphere.txt");

	BMP output1;
	output1.SetSize(test1->rays->width, test1->rays->height);
	output1.SetBitDepth(24);
	gVector4 ray1;

	//calculate the color for each of the pixels
	for(unsigned int x = 0; x < test1->rays->width;++x ) {
		for(unsigned int y = 0; y < test1->rays->height; y++) {
			ray1 = test1->color(x, y);
			if (ray1[0] >=1){
				output1(x, test1->rays->height-y-1)->Red = 255;
			} else {
				output1(x, test1->rays->height-y-1)->Red = std::abs(ray1[0])*255;
			}
			if (ray1[2] >=1){
				output1(x, test1->rays->height-y-1)->Blue = 255;
			} else {
				output1(x, test1->rays->height-y-1)->Blue = std::abs(ray1[2])*255;
			}
			if (ray1[1] >=1){
				output1(x, test1->rays->height-y-1)->Green = 255;
			} else {
				output1(x, test1->rays->height-y-1)->Green = std::abs(ray1[1])*255;
			}
		}
		
	}

	output1.WriteToFile(test1->outputFile.c_str());
	std::cout << "*********************************************" << std::endl;


	std::cout << "***** Testing Colored Clouds*****" << std::endl;
	//text file to read from
	test2 = ray_marching::factory("coloredClouds.txt");

	BMP output2;
	output2.SetSize(test2->rays->width, test2->rays->height);
	output2.SetBitDepth(24);
	gVector4 ray2;

	//calculate the color for each of the pixels
	for(unsigned int x = 0; x < test2->rays->width;++x ) {
		for(unsigned int y = 0; y < test2->rays->height; y++) {
			ray2 = test2->color(x, y);
			if (ray2[0] >=1){
				output2(x, test2->rays->height-y-1)->Red = 255;
			} else {
				output2(x, test2->rays->height-y-1)->Red = std::abs(ray2[0])*255;
			}
			if (ray2[2] >=1){
				output2(x, test2->rays->height-y-1)->Blue = 255;
			} else {
				output2(x, test2->rays->height-y-1)->Blue = std::abs(ray2[2])*255;
			}
			if (ray2[1] >=1){
				output2(x, test2->rays->height-y-1)->Green = 255;
			} else {
				output2(x, test2->rays->height-y-1)->Green = std::abs(ray2[1])*255;
			}
		}
		
	}

	output2.WriteToFile(test2->outputFile.c_str());
	std::cout << "*********************************************" << std::endl;

	std::cout << "***** Testing White Puff *****" << std::endl;
	//text file to read from
	test3 = ray_marching::factory("whitePuff.txt");

	BMP output3;
	output3.SetSize(test3->rays->width, test3->rays->height);
	output3.SetBitDepth(24);
	gVector4 ray3;

	//calculate the color for each of the pixels
	for(unsigned int x = 0; x < test3->rays->width;++x ) {
		for(unsigned int y = 0; y < test3->rays->height; y++) {
			ray3 = test3->color(x, y);
			if (ray3[0] >=1){
				output3(x, test3->rays->height-y-1)->Red = 255;
			} else {
				output3(x, test3->rays->height-y-1)->Red = std::abs(ray3[0])*255;
			}
			if (ray3[2] >=1){
				output3(x, test3->rays->height-y-1)->Blue = 255;
			} else {
				output3(x, test3->rays->height-y-1)->Blue = std::abs(ray3[2])*255;
			}
			if (ray3[1] >=1){
				output3(x, test3->rays->height-y-1)->Green = 255;
			} else {
				output3(x, test3->rays->height-y-1)->Green = std::abs(ray3[1])*255;
			}
		}
		
	}

	output3.WriteToFile(test3->outputFile.c_str());
	std::cout << "*********************************************" << std::endl;

	return 0;
}
