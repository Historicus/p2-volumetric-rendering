/**********************************************
* Author: Samuel Kenney 3/13/2017
*
* Grove City College: Introduction to Graphics
*
* Dr. Boatright
*
* Implements header for testing Volumetric Rendering class
*
**********************************************/

#pragma once
#include "VoxelBuffer.h"
#include "gVector4.h"
#include "perlin.h"
#include <cmath>
#include <random>
#include <time.h>


class volumetric_renderer
{
private:
	gVector4 center;
	float radius;
	std::string type;
	Perlin* noise;
	float delt;
public:
	volumetric_renderer(gVector4, float, std::string, float);
	~volumetric_renderer(void);
	//returns density for sphere at specific point
	float sphereDensity(gVector4);
	//returns density for cloud at specific point
	float cloudDensity(gVector4);
	//returns density for puff at specific point
	float puffDensity(gVector4);
	//genereate densities for the buffer and then return it
	VoxelBuffer* generateDensities(VoxelBuffer*);
};

