#pragma once

/**
 * This is the header file for a basic voxel buffer with the required functionality.
 * You may edit this as you wish provided these specific functions match their original form exactly.
 * This will aid in efficiently testing your submission by having a common ground from which to build
 * tests.
 * 
 * Cory D. Boatright, PhD
 * Created Jan. 22, 2015
 * Last modified: Feb. 1, 2017
 * COMP 361
 */

#include "ivec3.h"
#include "gVector4.h"
#include "gMatrix4.h"
#include <string>
#include <fstream>
#include <iostream>
#include <utility>

class VoxelBuffer {

private:
	
	//need ints for XYZ coordinates
	ivec3 xyzCoords;
	//need float to hold Delta
	float delta;

public:
	int height, width, depth;
	//need 1D array of float values
	std::pair<float, float> *voxelBuffer;
	//constructor for Voxel Buffer class
	VoxelBuffer(float delta, const ivec3& dimensions, int, int, int);

	//destructor for Voxel Buffer class
	~VoxelBuffer(void);

	//grabs density value when given coordinates
	float densityRead(const gVector4& coords) const;

	//grabs light value when given coordinates
	float lightRead(const gVector4& coords) const;

	//writes new density value when given coordinates
	void densityWrite(const gVector4& coords, float value);

	//writes new light value when given coordinates
	void lightWrite(const gVector4& coords, float value);

	//returns coordinates of center of the voxel specified with coordinates
	gVector4 getVoxelCenter(const gVector4& coords) const;

	//returns coordinates of center of the voxel specified with indices
	gVector4 getVoxelCenter(const ivec3& coords) const;
};