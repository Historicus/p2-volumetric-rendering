/**********************************************
* Author: Samuel Kenney 3/13/2017
*
* Grove City College: Introduction to Graphics
*
* Dr. Boatright
*
* Implements cpp file for testing Volumetric Rendering class
*
**********************************************/

#include "volumetric_renderer.h"


volumetric_renderer::volumetric_renderer(gVector4 c, float r, std::string t, float d)
{
	//(int octaves,float freq,float amp,int seed)
	srand (time(NULL));
	int seed = rand() % 1000000;
	noise = new Perlin(16, 0.07f, 1.5f, seed);
	center = c;
	radius = r;
	type = t;
	delt = d;
}


volumetric_renderer::~volumetric_renderer(void)
{
	delete noise;
}

float volumetric_renderer::sphereDensity(gVector4 point){
	//creates a sphere but it is stretched
	if (((std::pow(point[0] - center[0],2) ) + (std::pow(point[1] - center[1],2) + (std::pow(point[2] - center[2],2)))) <= std::pow(radius, 2)) {
		return 0.25f;
	}
	return 0.0f;
}

//returns density for cloud at specific point
float volumetric_renderer::cloudDensity(gVector4 point){
	//gVector4 newPoint = (point - center);
	float N = (noise->Get(point[0] - center[0], point[1] - center[1], point[2] - center[2])) + (1-(((point - center).length())/radius));
	if (N < 0.0f){
		return 0.0f;
	}

	return N;
}

//returns density for puff at specific point
float volumetric_renderer::puffDensity(gVector4 point){
	float maxValue = std::max(((radius - (((point - center).length())/radius) + std::abs(noise->Get(point[0] - center[0], point[1] - center[1], point[2] - center[2])))), 0.0f);

	return maxValue;
}

//genereate densities for the buffer and then return it
VoxelBuffer* volumetric_renderer::generateDensities(VoxelBuffer* buffer){
	ivec3 coords;
	//need to make sure to account for three different types of shapes
	int typeNum;
	if (type == "sphere"){
		typeNum = 0;
	} else if (type == "cloud"){
		typeNum = 1;
	} else {
		typeNum = 2;
	}

	switch (typeNum) {
	//sphere
	case 0:
		//iterate through each value
		for (int i = 0; i < buffer->width; i++)
		{
			for (int j = 0; j < buffer->height; j++)
			{
				for (int k = 0; k < buffer->depth; k++)
				{
					//convert to ivec3 struct
					coords.x = i;
					coords.y = j;
					coords.z = k;

					//calculate new density value and add it to the density value		
					gVector4 voxelCenter = buffer->getVoxelCenter(coords);
					float orig = buffer->densityRead(voxelCenter);
					float density = sphereDensity(voxelCenter) + orig;
					buffer->densityWrite(voxelCenter,density );
				}
			}
		}
		return buffer;
	//cloud
	case 1:
		//iterate through each value
		for (int i = 0; i < buffer->depth; i++)
		{
			for (int j = 0; j < buffer->height; j++)
			{
				for (int k = 0; k < buffer->width; k++)
				{
					//convert to ivec3 struct
					coords.x = i;
					coords.y = j;
					coords.z = k;

					//calculate new density value and add it to the density value
					gVector4 voxelCenter = buffer->getVoxelCenter(coords);
					float orig = buffer->densityRead(voxelCenter);
					float density = cloudDensity(voxelCenter) + orig;
					buffer->densityWrite(voxelCenter,density );
				}
			}
			
		}
		return buffer;
	//puff
	case 2:
		//iterate through each value
		for (int i = 0; i < buffer->depth; i++)
		{
			for (int j = 0; j < buffer->height; j++)
			{
				for (int k = 0; k < buffer->width; k++)
				{
					//convert to ivec3 struct
					coords.x = i;
					coords.y = j;
					coords.z = k;

					//calculate new density value and add it to the density value
					gVector4 voxelCenter = buffer->getVoxelCenter(coords);
					float orig = buffer->densityRead(voxelCenter);
					float density = puffDensity(voxelCenter) + orig;
					buffer->densityWrite(voxelCenter,density );
				}
			}
		}
		return buffer;
	}
}
