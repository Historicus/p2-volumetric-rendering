/**********************************************
* Author: Samuel Kenney 2/16/2017, modified 2/17/2017
*
* Grove City College: Introduction to Graphics
*
* Dr. Boatright
*
* Ray Generation Class
*
**********************************************/

#pragma once
#include <string>
#include "gVector4.h"
#include <iostream>
#include <fstream>
#include <utility>
#include <cmath>

#define PI 3.14159265f

class ray_generator
{
private:



	

	//view direction- n
	gVector4 vdir;

	//up vector
	gVector4 upvec;

	//unit vector for n
	gVector4 nHat;

	//vector from crossing nHat and up vector
	gVector4 uvec;

	//unit vector for u
	gVector4 uHat;

	//unit vector for up
	gVector4 upHat;

	//angle for FOVY
	float phi;

	//angle for FOYX
	float theta;




public:
	//vector from M to top
	gVector4 V;

	//vector from M to side
	gVector4 H;

	//center point of screen
	gVector4 M;
	//position of the eye
	gVector4 eyePos;

	//string for bitmap filename
	std::string BMPfilename;	
	float height;
	float width;
	//constructor
	ray_generator(std::string, int, int, gVector4, gVector4, gVector4, float);
	//destructor
	~ray_generator(void);
	//returns First Basis Vector
	void FirstBasisVector(void);
	//returns Second Basis Vector
	void SecondBasisVector(void);
	//returns Third Basis Vector
	void ThirdBasisVector(void);
	//calculate M, V, and H
	void calculateMVH(void);
	//generates ray with given x and y values- the rest are in class
	gVector4 generateRay(int, int);
};

